/*
 *  Função para validação do CPF.
 *  Recebe um argumento, o número do CPF com oito a onze dígitos.
 *  Retorna verdade se os dígitos verificadores do CPF estiverem corretos,
 *  falso caso contrário, ou nulo se o argumento for nulo, não tiver entre
 *  8 e 11 dígitos, ou contiver um dígito não numérico.
 *  Não são considerados válidos os CPF com todos os dígitos iguais:
 *  000.000.000-00 a 999.999.999-99.
 */

#include "postgres.h"
#include "fmgr.h"

#define VALOR(char) ((char) - '0')

PG_MODULE_MAGIC;
PG_FUNCTION_INFO_V1(rfb_cpf);

Datum
rfb_cpf(PG_FUNCTION_ARGS) {
    text *num;          // Único argumento = número do CPF
    bool iguais;        // Todos os dígitos são iguais
    int  fator,         // Fator de multiplicação
         digito;        // Dígito verificador
    char *cpf;          // Número do CPF com 11 dígitos
    char *c;            // Ponteiro para os caracteres do CPF
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o CPF tem entre 8 e 11 dígitos */
    if ( ((VARSIZE(num) - VARHDRSZ) > 11*sizeof(char)) ||
         ((VARSIZE(num) - VARHDRSZ) <  8*sizeof(char))
       ) PG_RETURN_NULL();
    /* CPF com 11 dígitos */
    cpf = (char *) palloc(11*sizeof(char));               // Reservar memória
    strncpy (cpf, "00000000000", 11*sizeof(char));        // Preencher com zeros
    memcpy (cpf+11*sizeof(char)-(VARSIZE(num)-VARHDRSZ),  // Destino
            VARDATA(num),                                 // Origem
            VARSIZE(num)-VARHDRSZ);                       // Comprimento
    /* Verificar se todos os dígitos são iguais */
    iguais = true;
    for (c=cpf; c<cpf+9*sizeof(char); *c++) {
        if (*c != *(c+sizeof(char))) {
           iguais = false;
           break;
        }
    }
    if (iguais) PG_RETURN_BOOL(false);
    /* Validar o primeiro dígito verificador */
    for (c=cpf, digito=0, fator=1; fator<10; fator++) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * fator;
    }
    digito %= 11;
    if (digito == 10) digito = 0; // Restos 0 ou 10 digito = 0
    /* Retornar nulo se o primeiro dígito verificador não for um dígito */
    if (!isdigit(*c)) PG_RETURN_NULL();
    // Retornar falso se o primeiro dígito não estiver correto
    if (digito != VALOR(*c)) PG_RETURN_BOOL(false);
    /* Validar o segundo dígito verificador */
    for (c=cpf, digito=0, fator=0; fator<10; fator++) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * fator;
    }
    digito %= 11;
    if (digito == 10) digito = 0; // Restos 0 ou 10 digito = 0
    /* Retornar nulo se o segundo dígito verificador não for um dígito */
    if (!isdigit(*c)) PG_RETURN_NULL();
    // Retornar verdade ou falso de acordo com o segundo dígito verificador
    PG_RETURN_BOOL (digito == VALOR(*c));
}