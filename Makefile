include := -I/usr/include/pgsql/server/

all: boleto complex funcs mdc rfb_cnpj rfb_cpf tse_nie

boleto: boleto.c
	cc -shared -o boleto.so -fPIC boleto.c $(include)
	sudo cp boleto.so /usr/lib64/pgsql/udf

complex: complex.c
	cc -shared -o complex.so -fPIC complex.c $(include)
	sudo cp complex.so /usr/lib64/pgsql/udf

funcs: funcs.c
	cc -shared -o funcs.so -fPIC funcs.c $(include)
	sudo cp funcs.so /usr/lib64/pgsql/udf

mdc: mdc.c
	cc -shared -o mdc.so -fPIC mdc.c $(include)
	sudo cp mdc.so /usr/lib64/pgsql/udf

rfb_cnpj: rfb_cnpj.c
	cc -shared -o rfb_cnpj.so -fPIC rfb_cnpj.c $(include)
	sudo cp rfb_cnpj.so /usr/lib64/pgsql/udf

rfb_cpf: rfb_cpf.c
	cc -shared -o rfb_cpf.so -fPIC rfb_cpf.c $(include)
	sudo cp rfb_cpf.so /usr/lib64/pgsql/udf

tse_nie: tse_nie.c
	cc -shared -o tse_nie.so -fPIC tse_nie.c $(include)
	sudo cp tse_nie.so /usr/lib64/pgsql/udf

clean:
	rm *.so
