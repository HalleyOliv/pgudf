DROP FUNCTION IF EXISTS mdc(int, int);

CREATE FUNCTION mdc(int, int) RETURNS int
    AS '$libdir/udf/mdc', 'mdc'
    LANGUAGE C STRICT;

SELECT mdc(15,20);

SELECT mdc(18, mdc(24,36));

SELECT mdc(252, 105);
