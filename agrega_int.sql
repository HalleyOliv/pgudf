--
-- Agregação com concatenação de números inteiros
--
CREATE OR REPLACE FUNCTION fun_estado_agrega_int(int[], int)
RETURNS int[] AS $$
    SELECT array_append($1, $2);
$$ LANGUAGE SQL STRICT;

CREATE OR REPLACE FUNCTION fun_final_agrega_int (int[])
RETURNS text AS $$
    SELECT array_to_string($1, ';');
$$ LANGUAGE SQL STRICT;

CREATE OR REPLACE AGGREGATE agrega_int
(
    BASETYPE = int,
    SFUNC = fun_estado_agrega_int,
    STYPE = int[],
    FINALFUNC = fun_final_agrega_int,
    INITCOND = '{}'
);

\pset null '(nulo)'
\pset border 2
\pset title 'Função agrega_int()'

SELECT sexo, agrega_int(altura_cm) AS alturas
FROM tbl_pessoas
GROUP BY sexo;

\pset title 'Função array_accum (anycompatible)'

SELECT sexo, array_accum(altura_cm) AS alturas
FROM tbl_pessoas
GROUP BY sexo;
