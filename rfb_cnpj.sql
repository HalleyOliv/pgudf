DROP FUNCTION IF EXISTS rfb_cnpj(text);

CREATE FUNCTION rfb_cnpj(text) RETURNS boolean
    AS '$libdir/udf/rfb_cnpj', 'rfb_cnpj'
    LANGUAGE C STRICT;

DROP TABLE IF EXISTS cnpj_gov_paraiba;

CREATE TABLE cnpj_gov_paraiba(
    cnpj  TEXT PRIMARY KEY,
    orgao TEXT)
;

-- Massa de teste:
-- Banco do Brasil - Governo Estadual da Paraíba
-- https://www.bb.com.br/pbb/pagina-inicial/seu-salario-no-bb/servidor-da-paraiba/cnpjs#/
\copy cnpj_gov_paraiba FROM '/tmp/cnpj_gov_paraiba.csv' (DELIMITER(';'));

SELECT cnpj, rfb_cnpj(cnpj)
FROM cnpj_gov_paraiba;
