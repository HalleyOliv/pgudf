/*
 *  Função para validação do número de inscrição eleitoral (nie).
 *  Recebe um argumento, o número de inscrição eleitoral.
 *  Retorna verdade se os dígitos verificadores estiverem corretos,
 *  falso caso contrário, ou nulo se o argumento for nulo (STRICT),
 *  não tiver entre 10 e 12 dígitos, ou tiver um dígito não
 *  numérico antes dos dígitos verificadores.
 */

#include "postgres.h"
#include "fmgr.h"

#define VALOR(char) ((char) - '0')

PG_MODULE_MAGIC;
PG_FUNCTION_INFO_V1(tse_nie);

Datum
tse_nie(PG_FUNCTION_ARGS) {
    text *num;          // Único argumento = número de inscrição eleitoral
    int  fator,         // Fator de multiplicação
         digito;        // Dígito verificador
    char *nie;          // Número do inscrição eleitoral com 12 dígitos
    char *c;            // Ponteiro para os caracteres do número de inscrição
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o número de inscrição tem entre 10 e 12 dígitos */
    if ( ((VARSIZE(num) - VARHDRSZ) > 12*sizeof(char)) ||
         ((VARSIZE(num) - VARHDRSZ) < 10*sizeof(char))
       ) PG_RETURN_NULL();
    /* Número de inscrição eleitoral com 12 dígitos */
    nie = (char *) palloc(12*sizeof(char));               // Reservar memória
    strncpy (nie, "000000000000", 12*sizeof(char));       // Completar com zeros
    memcpy (nie+12*sizeof(char)-(VARSIZE(num)-VARHDRSZ),  // Destino
            VARDATA(num),                                 // Origem
            VARSIZE(num)-VARHDRSZ);                       // Comprimento
    /* Validar o primeiro dígito verificador */
    for (c=nie, digito=0, fator=9; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    // Retornar falso se o primeiro dígito não estiver correto
    if (digito != VALOR(*(c+2*sizeof(char)))) PG_RETURN_BOOL(false);
    /* Validar o segundo dígito verificador */
    for (digito=0, fator=4; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 -> digito = 0
    // Retornar verdade ou falso de acordo com o segundo dígito verificador
    PG_RETURN_BOOL (digito == VALOR(*c));
}