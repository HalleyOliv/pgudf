---------------------------------------------------------------------------
--
-- dvrfb--1.0.sql
--
--    Funções para validar dígitos verificadores do CPF e do CNPJ,
--    conforme especificado pela Receita Federal do Brasil.
--
---------------------------------------------------------------------------

-- reclamar se esse script estiver sendo executado no psql,
-- em vez de via CREATE EXTENSION
\echo Use "CREATE EXTENSION dvrfb" para carregar esse arquivo. \quit

COMMENT ON EXTENSION dvrfb
    IS 'Funções para validar dígitos verificadores do CPF e do CNPJ';

CREATE FUNCTION rfb_cpf(text) RETURNS boolean
    AS '$libdir/udf/rfb_cpf', 'rfb_cpf'
    LANGUAGE C STRICT;

CREATE FUNCTION rfb_cnpj(text) RETURNS boolean
    AS '$libdir/udf/rfb_cnpj', 'rfb_cnpj'
    LANGUAGE C STRICT;
