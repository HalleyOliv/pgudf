-- DAC do Código de Barras
DROP FUNCTION IF EXISTS daccb(text);

CREATE FUNCTION daccb(text) RETURNS text
    AS '$libdir/udf/boleto', 'daccb'
    LANGUAGE C STRICT;

SELECT daccb('3419166700000123451101234567880057123457000');

-- DAC da Representação Numérica
DROP FUNCTION IF EXISTS dacrn(text);

CREATE FUNCTION dacrn(text) RETURNS text
    AS '$libdir/udf/boleto', 'dacrn'
    LANGUAGE C STRICT;

SELECT dacrn('341911012');

SELECT dacrn('3456788005');
