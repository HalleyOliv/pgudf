/*
 *  Rotina para validação do CNPJ.
 *  Recebe um argumento, o número do CNPJ com quatorze dígitos.
 *  Retorna verdade se os dígitos verificadores do CNPJ estiverem corretos,
 *  falso caso contrário, ou nulo se o argumento for nulo, não tiver 14 dígitos,
 *  ou tiver um dígito não numérico.
 */

#include "postgres.h"
#include "fmgr.h"

#define VALOR(char) ((char) - '0')

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(rfb_cnpj);
Datum
rfb_cnpj(PG_FUNCTION_ARGS) {
    text *num;          // Único argumento = número do CNPJ
    int  fator,         // Fator de multiplicação
         digito;        // Dígito verificador
    char *c;            // Ponteiro para os caracteres do CNPJ
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o CNPJ tem 14 dígitos */
    if ( (VARSIZE(num) - VARHDRSZ) != 14*sizeof(char) ) {
       PG_RETURN_NULL();
    }
    /* Validar o primeiro dígito verificador */
    for (c=VARDATA(num), digito=0, fator=1; fator<13; fator++) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * (fator<5 ? fator+5 : fator-3);
    }
    if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se o DV não for dígito
    /* Calcular o primeiro dígito verificador Módulo 11*/
    digito %= 11;
    if (digito == 10) digito = 0; // Restos 0 ou 1 digito = 0
    /* Retornar falso se o primeiro dígito não estiver correto */
    if (digito != VALOR(*c)) PG_RETURN_BOOL(false);
    /* Validar o segundo dígito verificador */
    for (c=VARDATA(num), digito=0, fator=1; fator<14; fator++) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se não for dígito
        digito += VALOR(*c++) * (fator<6 ? fator+4 : fator-4);
    }
    if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se o DV não for dígito
    /* Calcular o segundo dígito verificador Módulo 11*/
    digito %= 11;
    if (digito == 10) digito = 0; // Restos 0 ou 1 digito = 0
    /* Retornar verdade ou falso de acordo com o segundo dígito verificador */
    PG_RETURN_BOOL (digito == VALOR(*c));
}
