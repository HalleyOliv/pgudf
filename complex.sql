------------------------------------------------------------------------
--
-- complex.sql
--    Esse arquivo mostra como criar um novo tipo de dados
--    definido pelo usuário, e como usar esse novo tipo.
--
--
-- Portions Copyright (c) 1996-2021, PostgreSQL Global Development Group
-- Portions Copyright (c) 1994, Regents of the University of California
--
-- Código-fonte do PostgreSQL: src/tutorial/complex.source
--
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Criação de novo tipo de dados:
--  Aqui é criado um novo tipo chamado 'complex', representando números
--  complexos.
--  O tipo definido pelo usuário deve ter função de entrada e saída
--  e, opcionalmente, pode ter funções de entrada e saída binárias.
--  Geralmente são todas funções escritas em C definidas pelo usuário.
------------------------------------------------------------------------

-- Suponha que as funções definidas pelo usuário estejam em
-- _OBJWD_/complex$DLSUFFIX (não se deseja assumir que estejam no
-- caminho de procura do carregador dinâmico).
-- O código-fonte se encontra em $PWD/complex.c.
-- Observe que todas as funções são declaradas como STRICT, portanto
-- não é necessário lidar com entradas NULL no código C.
-- Também são marcadas como IMMUTABLE, porque sempre retornam as
-- mesmas saídas dadas as mesmas entradas.

-- a função de entrada 'complex_in' pega uma cadeia de caracteres
-- terminada em nulo (a representação textual do tipo), e a transforma
-- na representação interna (na memória).
-- Será recebida uma mensagem informando que 'complex' ainda não
-- existe, mas tudo bem.

CREATE FUNCTION complex_in(cstring)
   RETURNS complex
   AS '_OBJWD_/complex'
   LANGUAGE C IMMUTABLE STRICT;

-- a função de saída 'complex_out' pega a representação interna
-- e a converte na representação textual.

CREATE FUNCTION complex_out(complex)
   RETURNS cstring
   AS '_OBJWD_/complex'
   LANGUAGE C IMMUTABLE STRICT;

-- a função de entrada binária 'complex_recv' pega o buffer StringInfo
-- e transforma seu conteúdo na representação interna.

CREATE FUNCTION complex_recv(internal)
   RETURNS complex
   AS '_OBJWD_/complex'
   LANGUAGE C IMMUTABLE STRICT;

-- a função de saída binária 'complex_send' pega a representação
-- interna e a converte em uma cadeia de caracteres bytea independente
-- de plataforma (espera-se).

CREATE FUNCTION complex_send(complex)
   RETURNS bytea
   AS '_OBJWD_/complex'
   LANGUAGE C IMMUTABLE STRICT;


-- agora, o tipo pode ser criado.
-- internallength especifica o tamanho do bloco de memória necessário
-- para manter o tipo (é necessária uma dupla de 8 bytes).

CREATE TYPE complex (
   internallength = 16,
   input = complex_in,
   output = complex_out,
   receive = complex_recv,
   send = complex_send,
   alignment = double
);


------------------------------------------------------------------------
-- Uso do novo tipo de dados:
--  tipos de dados definidos pelo usuário podem ser usados
--  como os tipos internos comuns.
------------------------------------------------------------------------

-- por exemplo. pode ser usado em uma tabela

CREATE TABLE test_complex (
    a   complex,
    b   complex
);

-- dados para tipos definidos pelo usuário são apenas cadeias de
-- caracteres na representação textual apropriada.

INSERT INTO test_complex VALUES ('(1.0, 2.5)', '(4.2, 3.55 )');
INSERT INTO test_complex VALUES ('(33.0, 51.4)', '(100.42, 93.55)');

SELECT * FROM test_complex;

------------------------------------------------------------------------
-- Criação de operador para o novo tipo de dados:
-- Definição do operador soma para tipos complexos.
-- Como o POSTGRES oferece suporte à sobrecarga de função, será usado
-- o símbolo + como operador de adição.
-- (Os nomes dos operadores podem ser reutilizados com diferentes
-- números e tipos de argumentos.)
------------------------------------------------------------------------

-- primeiro, será definida a função complex_add (também em complex.c)
CREATE FUNCTION complex_add(complex, complex)
   RETURNS complex
   AS '_OBJWD_/complex'
   LANGUAGE C IMMUTABLE STRICT;

-- Agora pode ser definido o operador.
-- Aqui será mostrado o operador binário, mas também pode ser definido
-- um operador de prefixo omitindo leftarg.
CREATE OPERATOR + (
   leftarg = complex,
   rightarg = complex,
   procedure = complex_add,
   commutator = +
);


SELECT (a + b) AS c FROM test_complex;

-- Ocasionalmente, pode-se achar útil converter a cadeia de caracteres
-- para o tipo desejado explicitamente. :: denota conversão de tipo.

SELECT  a + '(1.0,1.0)'::complex AS aa,
        b + '(1.0,1.0)'::complex AS bb
   FROM test_complex;


------------------------------------------------------------------------
-- Criação das funções de agregação
--  Também podem ser definidas funções de agregação.
--  A sintaxe é um tanto enigmática, mas a ideia é expressar
--  a agregação em termos de funções de transição de estado.
------------------------------------------------------------------------

CREATE AGGREGATE complex_sum (
   sfunc = complex_add,
   basetype = complex,
   stype = complex,
   initcond = '(0,0)'
);

SELECT complex_sum(a) FROM test_complex;


------------------------------------------------------------------------
-- Interface do novo tipo com índices:
--  Ainda não é possivel definir um índice secundário
--  (por exemplo, uma árvore B) sobre o novo tipo.
--  É preciso criar todos os operadores e funções de suporte
--  necessários, para então poder criar a classe de operador.
------------------------------------------------------------------------

-- primeiro, definir os operadores necessários
CREATE FUNCTION complex_abs_lt(complex, complex) RETURNS bool
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION complex_abs_le(complex, complex) RETURNS bool
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION complex_abs_eq(complex, complex) RETURNS bool
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION complex_abs_ge(complex, complex) RETURNS bool
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;
CREATE FUNCTION complex_abs_gt(complex, complex) RETURNS bool
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;

CREATE OPERATOR < (
   leftarg = complex, rightarg = complex, procedure = complex_abs_lt,
   commutator = > , negator = >= ,
   restrict = scalarltsel, join = scalarltjoinsel
);
CREATE OPERATOR <= (
   leftarg = complex, rightarg = complex, procedure = complex_abs_le,
   commutator = >= , negator = > ,
   restrict = scalarlesel, join = scalarlejoinsel
);
CREATE OPERATOR = (
   leftarg = complex, rightarg = complex, procedure = complex_abs_eq,
   commutator = = ,
   -- deixar de fora o negador, já que não foi criado o operador <>
   -- negador = <> ,
   restrict = eqsel, join = eqjoinsel
);
CREATE OPERATOR >= (
   leftarg = complex, rightarg = complex, procedure = complex_abs_ge,
   commutator = <= , negator = < ,
   restrict = scalargesel, join = scalargejoinsel
);
CREATE OPERATOR > (
   leftarg = complex, rightarg = complex, procedure = complex_abs_gt,
   commutator = < , negator = <= ,
   restrict = scalargtsel, join = scalargtjoinsel
);

-- criar a função de suporte também
CREATE FUNCTION complex_abs_cmp(complex, complex) RETURNS int4
   AS '_OBJWD_/complex' LANGUAGE C IMMUTABLE STRICT;

-- agora pode ser criada a classe de operador
CREATE OPERATOR CLASS complex_abs_ops
    DEFAULT FOR TYPE complex USING btree AS
        OPERATOR        1       < ,
        OPERATOR        2       <= ,
        OPERATOR        3       = ,
        OPERATOR        4       >= ,
        OPERATOR        5       > ,
        FUNCTION        1       complex_abs_cmp(complex, complex);


-- e agora pode ser definido um índice btree em tipos complexos.
-- Primeiro, a tabela deve ser preenchida.
-- Observe que o postgres precisa de muito mais tuplas para
-- começar a usar o índice btree durante as seleções.
INSERT INTO test_complex VALUES ('(56.0,-22.5)', '(-43.2,-0.07)');
INSERT INTO test_complex VALUES ('(-91.9,33.6)', '(8.6,3.0)');

CREATE INDEX test_cplx_ind ON test_complex
   USING btree(a complex_abs_ops);

SELECT * from test_complex where a = '(56.0,-22.5)';
SELECT * from test_complex where a < '(56.0,-22.5)';
SELECT * from test_complex where a > '(56.0,-22.5)';


-- apagar o exemplo
DROP TABLE test_complex;
DROP TYPE complex CASCADE;
