Este projeto abriga funções definidas pelo usuário, que podem ser compiladas em objetos dinamicamente carregáveis (também chamados de bibliotecas compartilhadas), e carregados pelo servidor PostgreSQL sob demanda.

Podem ser encontradas mais informações no [Apêndice P. Biblioteca de funções definidas pelo usuário](https://pgdocptbr.xyz/pgudf-appendix.html), da tradução da documentação do PostgreSQL para o Português do Brasil.

