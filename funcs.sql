---------------------------------------------------------------------------
--
-- funcs.sql-
--      Tutorial sobre o uso de funções no POSTGRES.
--
--
-- Copyright (c) 1994-5, Regents of the University of California
--
-- src/tutorial/funcs.source
--
---------------------------------------------------------------------------

---------------------------------------------------------------------------
-- Criação de funções SQL em tipos base
-- A instrução CREATE FUNCTION permite criar uma função que pode ser usada
-- em expressões (SELECT, INSERT, etc.).
-- Começaremos com funções que retornam valores de tipos base.
---------------------------------------------------------------------------

--
-- Vamos criar uma função SQL simples que não receba argumentos, e retorna 1

CREATE FUNCTION one() RETURNS integer
   AS 'SELECT 1 as ONE' LANGUAGE SQL;

--
-- As funções podem ser usadas em quaisquer expressões
-- (por exemplo, na lista de destino ou qualificações)

SELECT one() AS um;

--
-- Aqui está como se cria uma função que recebe argumentos.
-- A função a seguir retorna a soma de seus dois argumentos:

CREATE FUNCTION add_em(integer, integer) RETURNS integer
   AS 'SELECT $1 + $2' LANGUAGE SQL;

SELECT add_em(1, 2) AS três;

---------------------------------------------------------------------------
-- Criação de funções SQL em tipos compostos
-- Também é possível criar funções que retornam valores de tipos compostos.
---------------------------------------------------------------------------

-- Antes de criar funções mais sofisticadas, vamos preencher a tabela EMP

CREATE TABLE EMP (
    name       text,
    salary     integer,
    age        integer,
    cubicle    point
);

INSERT INTO EMP VALUES ('Sam', 1200, 16, '(1,1)');
INSERT INTO EMP VALUES ('Claire', 5000, 32, '(1,2)');
INSERT INTO EMP VALUES ('Andy', -1000, 2, '(1,3)');
INSERT INTO EMP VALUES ('Bill', 4200, 36, '(2,1)');
INSERT INTO EMP VALUES ('Ginger', 4800, 30, '(2,4)');

-- O argumento de uma função também pode ser uma tupla.
-- Por exemplo, a função double_salary recebe uma tupla da tabela EMP

CREATE FUNCTION double_salary(EMP) RETURNS integer
   AS 'SELECT $1.salary * 2 AS salary' LANGUAGE SQL;

SELECT name, double_salary(EMP) AS dream
FROM EMP
WHERE EMP.cubicle ~= '(2,1)'::point;

-- O valor retornado por uma função também pode ser uma tupla.
-- Entretanto, devemos nos certificar de que as expressões na
-- lista de destino estejam na mesma ordem das colunas de EMP.

CREATE FUNCTION new_emp() RETURNS EMP
   AS 'SELECT ''None''::text AS name,
              1000 AS salary,
              25 AS age,
              ''(2,2)''::point AS cubicle'
   LANGUAGE SQL;

-- Pode-se então projetar uma coluna a partir da tupla resultante
-- usando a "notação de função" para colunas de projeção.
-- (ou seja. bar(foo) é equivalente a foo.bar)
-- Observe que não é oferecido suporte a new_emp().name no momento.

SELECT name(new_emp()) AS nobody;

-- Vamos experimentar mais uma função que retorna tuplas
CREATE FUNCTION high_pay() RETURNS setof EMP
   AS 'SELECT * FROM EMP where salary > 1500'
   LANGUAGE SQL;

SELECT name(high_pay()) AS overpaid;


---------------------------------------------------------------------------
-- Ao criar funções SQL com várias instruções SQL, também podem ser criadas
-- funções que fazem mais do que apenas um SELECT.
---------------------------------------------------------------------------

-- Deve-se ter notado que Andy tem salário negativo.
-- Vamos criar uma função que remove funcionários com salários negativos.

SELECT * FROM EMP;

CREATE FUNCTION clean_EMP () RETURNS integer
   AS 'DELETE FROM EMP WHERE EMP.salary <= 0;
       SELECT 1 AS ignore_this'
   LANGUAGE SQL;

SELECT clean_EMP();

SELECT * FROM EMP;

---------------------------------------------------------------------------
-- Criação de funções na linguagem C
-- Além das funções SQL, também podem ser criadas funções C.
-- Veja no arquivo funcs.c a definição das funções C.
---------------------------------------------------------------------------

DROP FUNCTION IF EXISTS add_one(integer);

CREATE FUNCTION add_one(integer) RETURNS integer
   AS '$libdir/udf/funcs' LANGUAGE C;

DROP FUNCTION IF EXISTS add_one(double precision);

CREATE FUNCTION add_one(double precision) RETURNS double precision AS
    '$libdir/udf/funcs', 'add_one_float8'
LANGUAGE C STRICT;

DROP FUNCTION IF EXISTS makepoint(point, point);

CREATE FUNCTION makepoint(point, point) RETURNS point
   AS '$libdir/udf/funcs' LANGUAGE C;

DROP FUNCTION IF EXISTS copytext(text);

CREATE FUNCTION copytext(text) RETURNS text
   AS '$libdir/udf/funcs' LANGUAGE C;

DROP FUNCTION IF EXISTS c_overpaid(EMP, integer);

CREATE FUNCTION c_overpaid(EMP, integer) RETURNS boolean
   AS '$libdir/udf/funcs' LANGUAGE C;

SELECT add_one(3) AS four;

SELECT add_one(1.5001e3);

SELECT makepoint('(1,2)'::point, '(3,4)'::point ) AS novoponto;

SELECT copytext('hello world!');

SELECT name, c_overpaid(EMP, 1500) AS sobrepago
FROM EMP
WHERE name = 'Bill' or name = 'Sam';

-- Remover os objetos criados nesse arquivo

DROP FUNCTION c_overpaid(EMP, integer);
DROP FUNCTION copytext(text);
DROP FUNCTION makepoint(point, point);
DROP FUNCTION add_one(integer);
DROP FUNCTION add_one(double precision);
DROP FUNCTION clean_EMP();
DROP FUNCTION high_pay();
DROP FUNCTION new_emp();
DROP FUNCTION add_em(integer, integer);
DROP FUNCTION one();
DROP FUNCTION double_salary(EMP);

DROP TABLE EMP;
