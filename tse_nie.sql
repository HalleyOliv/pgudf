DROP FUNCTION IF EXISTS tse_nie(text);

CREATE FUNCTION tse_nie(text) RETURNS boolean
    AS '$libdir/udf/tse_nie', 'tse_nie'
    LANGUAGE C STRICT;

\pset null <nulo>

SELECT tse_nie(NULL);

