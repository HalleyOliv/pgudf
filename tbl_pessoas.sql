--
-- Tabela usada pelas agregações definidas pelo usuário
--
DROP TABLE IF EXISTS tbl_pessoas;
CREATE TABLE tbl_pessoas (
    id          SERIAL,
    nome        TEXT,
    sobrenome   TEXT,
    sexo        CHAR(1),
    altura_cm   INT
);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Ana','Maria','F',175);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Manoel','Pacheco','M',168);
INSERT INTO tbl_pessoas VALUES (DEFAULT,NULL,'Barbosa',NULL,NULL);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Doroteia','Braga',NULL,NULL);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Manoel','Oliveira','M',181);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Ana','Fraga','F',165);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Maria','Pereira','F',164);
INSERT INTO tbl_pessoas VALUES (DEFAULT,'Manoel','Silva','M',153);
\pset null '(nulo)'
\pset border 2
\pset title 'Tabela tbl_pessoas'
SELECT * FROM tbl_pessoas;
