DROP FUNCTION IF EXISTS rfb_cpf(text);

CREATE FUNCTION rfb_cpf(text) RETURNS boolean
    AS '$libdir/udf/rfb_cpf', 'rfb_cpf'
    LANGUAGE C STRICT;

\pset null <nulo>

SELECT rfb_cpf(NULL);

SELECT rfb_cpf('28001238938');

