/*
 *  Cobrança bancária.
 *  Rotinas para cálculo dos dígitos verifcadores (DAC) dos boletos.
 */

#include "postgres.h"
#include "fmgr.h"

#define VALOR(char) ((char) - '0')
#define DIGITO(val)   ((val) + '0')
#define VAREND(__PTR) (VARDATA(__PTR) + VARSIZE(__PTR) - VARHDRSZ - sizeof(char))

PG_MODULE_MAGIC;

/*
 *  Cálculo do dígito verificador (DAC) da representação
 *  numérica, por meio do Módulo 10.
 *  Recebe um argumento, o número cujo dígito verificador
 *  será calculado, na forma de texto.
 *  Retorna o dígito verificador, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(dacrn);

Datum
dacrn(PG_FUNCTION_ARGS) {
    int  digito = 0,    // Dígito verificador
         fator  = 2,    // Fator de multiplicação
         produto;       // Produto = dígito x fator
    text *num;          // Único argumento = número
    char *c;            // Ponteiro para os caracteres do número
    int32 tamanho;      // Tamanho do resultado da função
    text *resultado;    // Ponteiro para o resultado da função
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar o recebimento de argumento vazio */
    if (VARSIZE(num) == VARHDRSZ) {
        PG_RETURN_NULL();
    }
    /* Calcular o dígito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se é dígito
        produto = VALOR(*c) * fator;
        digito+= produto/10 + produto%10;
        if (--fator < 1) fator = 2;
    }
    digito = 10 - ( digito % 10 );
    if (digito == 10) digito = 0;
    /* Retornar o dígito verificador */
    tamanho = VARHDRSZ + sizeof(char);
    resultado = (text *) palloc(tamanho);
    memset((void *) resultado, 0, tamanho);
    SET_VARSIZE(resultado, tamanho);
    *VARDATA(resultado) = (char) DIGITO(digito);
    PG_RETURN_TEXT_P(resultado);
}

/*
 *  Cálculo do dígito verificador (DAC) do código de barras,
 *  por meio do Módulo 11, com base de cálculo igual a 9.
 *  Recebe um argumento, o número cujo dígito verificador
 *  será calculado, na forma de texto.
 *  Retorna o dígito verificador, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(daccb);

Datum
daccb(PG_FUNCTION_ARGS) {
    int  digito=0,      // Dígito verificador
         fator=2;       // Fator de multiplicação
    text *num;          // Único argumento = número
    char *c;            // Ponteiro para os caracteres do número
    int32 tamanho;      // Tamanho do resultado da função
    text *resultado;    // Ponteiro para o resultado da função
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar o recebimento de argumento vazio */
    if (VARSIZE(num) == VARHDRSZ) {
        PG_RETURN_NULL();
    }
    /* Calcular o dígito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se é dígito
        digito += VALOR(*c) * fator;
        if (++fator > 9) fator = 2;
    }
    digito = 11 - ( digito % 11 );
    if (digito == 0 || digito == 1 || digito > 9) digito = 1;
    /* Retornar o dígito verificador */
    tamanho = VARHDRSZ + sizeof(char);
    resultado = (text *) palloc(tamanho);
    memset((void *) resultado, 0, tamanho);
    SET_VARSIZE(resultado, tamanho);
    *VARDATA(resultado) = (char) DIGITO(digito);
    PG_RETURN_TEXT_P(resultado);
}
