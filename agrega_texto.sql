--
-- Agregação com concatenação de texto
--
CREATE OR REPLACE FUNCTION fun_estado_agrega_texto (text[], text)
RETURNS text[] AS $$
    SELECT array_append($1, $2);
$$ LANGUAGE SQL STRICT;

CREATE OR REPLACE FUNCTION fun_final_agrega_texto (text[])
RETURNS text AS $$
    SELECT array_to_string($1, ';');
$$ LANGUAGE SQL STRICT;

CREATE OR REPLACE AGGREGATE agrega_texto
(
    BASETYPE=text,
    SFUNC = fun_estado_agrega_texto,
    STYPE = text[],
    FINALFUNC = fun_final_agrega_texto,
    INITCOND = '{}'
);

\pset null '(nulo)'
\pset border 2
\pset title 'Nomes e sobrenomes das pessoas agrupados por sexo'

SELECT sexo, agrega_texto(nome || ' ' || sobrenome) AS pessoas
FROM tbl_pessoas
GROUP BY sexo;

\pset title 'Função array_accum (anycompatible)'

SELECT sexo, array_accum(nome || ' ' || sobrenome) AS pessoas
FROM tbl_pessoas
GROUP BY sexo;
