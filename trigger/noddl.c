#include "postgres.h"
#include "fmgr.h"
#include "commands/event_trigger.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(noddl);

Datum
noddl(PG_FUNCTION_ARGS)
{
    EventTriggerData *trigdata;

    if (!CALLED_AS_EVENT_TRIGGER(fcinfo))  /* erro interno */
        elog(ERROR, "noddl: não foi chamada pelo gerenciador de gatilho de evento");

    trigdata = (EventTriggerData *) fcinfo->context;

    ereport(ERROR,
        (errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
                 errmsg("comando \"%s\" negado", GetCommandTagName(trigdata->tag))));

    PG_RETURN_NULL();
}
