#include "postgres.h"
#include "fmgr.h"
#include "executor/spi.h"       /* necessário para trabalhar com SPI */
#include "commands/trigger.h"   /* ... gatilhos ... */
#include "utils/rel.h"          /* ... e relações */

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(trigf);

Datum
trigf(PG_FUNCTION_ARGS)
{
    TriggerData *trigdata = (TriggerData *) fcinfo->context;
    TupleDesc   tupdesc;
    HeapTuple   rettuple;
    char       *when;
    bool        checknull = false;
    bool        isnull;
    int         ret, i;

    /* certifique-se de que é chamado como um gatilho */
    if (!CALLED_AS_TRIGGER(fcinfo))
        elog(ERROR, "trigf: não foi chamada pelo gerenciador de gatilho");

    /* tupla para retornar ao executor */
    if (TRIGGER_FIRED_BY_UPDATE(trigdata->tg_event))
        rettuple = trigdata->tg_newtuple;
    else
        rettuple = trigdata->tg_trigtuple;

    /* verificar valores nulos */
    if (!TRIGGER_FIRED_BY_DELETE(trigdata->tg_event)
        && TRIGGER_FIRED_BEFORE(trigdata->tg_event))
        checknull = true;

    if (TRIGGER_FIRED_BEFORE(trigdata->tg_event))
        when = "before";
    else
        when = "after ";

    tupdesc = trigdata->tg_relation->rd_att;

    /* conectar ao gerenciador SPI */
    if ((ret = SPI_connect()) < 0)
        elog(ERROR, "trigf (disparado %s): SPI_connect returned %d", when, ret);

    /* obter o número de linhas na tabela */
    ret = SPI_exec("SELECT count(*) FROM ttest", 0);

    if (ret < 0)
        elog(ERROR, "trigf (disparado %s): SPI_exec retornou %d", when, ret);

    /* count(*) retorna int8, então tenha o cuidado de converter */
    i = DatumGetInt64(SPI_getbinval(SPI_tuptable->vals[0],
                                    SPI_tuptable->tupdesc,
                                    1,
                                    &isnull));

    elog (INFO, "trigf (disparado %s): existem %d linhas em ttest", when, i);

    SPI_finish();

    if (checknull)
    {
        SPI_getbinval(rettuple, tupdesc, 1, &isnull);
        if (isnull)
            rettuple = NULL;
    }

    return PointerGetDatum(rettuple);
}
