DROP FUNCTION IF EXISTS trigf();

CREATE FUNCTION trigf() RETURNS trigger
    AS '$libdir/udf/trigf', 'trigf'
    LANGUAGE C;
