DROP EVENT TRIGGER IF EXISTS noddl;

DROP TABLE IF EXISTS foo;

DROP FUNCTION IF EXISTS noddl();

CREATE FUNCTION noddl() RETURNS event_trigger
    AS '$libdir/udf/noddl', 'noddl'
    LANGUAGE C;

CREATE EVENT TRIGGER noddl ON ddl_command_start
    EXECUTE FUNCTION noddl();

CREATE TABLE foo(id serial);

-- Desativar o gatilho apenas durante a transação

BEGIN;
ALTER EVENT TRIGGER noddl DISABLE;
CREATE TABLE foo (id serial);
ALTER EVENT TRIGGER noddl ENABLE;
COMMIT;
