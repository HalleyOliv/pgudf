CREATE OR REPLACE FUNCTION no_rewrite()
 RETURNS event_trigger
 LANGUAGE plpgsql AS
$$
---
--- Implementação de política de reescrita de tabela local:
---   a tabela public.foo não tem permissão para ser reescrita, nunca;
---   as demais tabelas só podem ser reescritas se tiverem  menos de
---   100 páginas, e assim mesmo apenas entre 1h e 6h da manhã.
---
DECLARE
  table_oid oid := pg_event_trigger_table_rewrite_oid();
  current_hour integer := extract('hour' from current_time);
  pages integer;
  max_pages integer := 100;
BEGIN
  IF pg_event_trigger_table_rewrite_oid() = 'public.foo'::regclass
  THEN
        RAISE EXCEPTION 'você não tem permissão para reescrever a tabela %',
                        table_oid::regclass;
  END IF;

  SELECT INTO pages relpages FROM pg_class WHERE oid = table_oid;
  IF pages > max_pages
  THEN
        RAISE EXCEPTION 'reescrita permitida apenas para tabelas com menos de % de páginas',
                        max_pages;
  END IF;

  IF current_hour NOT BETWEEN 1 AND 6
  THEN
        RAISE EXCEPTION 'reescrita permitida apenas entre 1h e 6h da manhã';
  END IF;
END;
$$;

DROP EVENT TRIGGER IF EXISTS no_rewrite_allowed;

CREATE EVENT TRIGGER no_rewrite_allowed
                  ON table_rewrite
   EXECUTE FUNCTION no_rewrite();

DROP TABLE IF EXISTS foo;
DROP TABLE IF EXISTS fuu;
CREATE TABLE foo(bar text);
INSERT INTO foo VALUES(repeat('1234567890',10));
CREATE TABLE fuu(bar text);
INSERT INTO fuu VALUES(repeat('1234567890',10));
ALTER TABLE foo ALTER COLUMN bar TYPE VARCHAR(100);
ALTER TABLE fuu ALTER COLUMN bar TYPE VARCHAR(100);
