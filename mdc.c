/*
 *  Rotina para cálculo do Máximo Divisor Comum (MDC).
 *  Utiliza o Algoritmo de Euclides ou Algoritmo Euclidiano.
 *  Recebe como argumento dois números inteiros e retorna o MDC.
 *  Fonte: https://pt.wikipedia.org/wiki/Algoritmo_de_Euclides
 */

#include "postgres.h"
#include "fmgr.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(mdc);

Datum
mdc(PG_FUNCTION_ARGS) {
    int  a,     // Primeiro número
         b,     // Segundo número
         r,     // Resto da divisão de A por B
         t;     // Armazenamento temporário (troca)
    /* Receber os argumentos */
    a = PG_GETARG_INT32(0);
    b = PG_GETARG_INT32(1);
    /* Garantir que "a" seja o maior valor */
    if ( a < b ) {
       t = a;
       a = b;
       b = t;
    }
    /* Calcular o MDC */
    while ( b != 0 )
    {
      r = a % b;
      a = b;
      b = r;
    }
    PG_RETURN_INT32(a);
}
