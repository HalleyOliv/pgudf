--
-- Agregação com geração de histograma
--
CREATE OR REPLACE FUNCTION fun_estado_agrega_histograma (text)
RETURNS text AS $$
    SELECT $1 || '*';
$$ LANGUAGE SQL STRICT;

CREATE OR REPLACE AGGREGATE agrega_histograma
(
    BASETYPE = "ANY",
    SFUNC = fun_estado_agrega_histograma,
    STYPE = text,
    INITCOND = ''
);

\pset null '(nulo)'
\pset border 2
\pset title 'Histograma dos nomes'

SELECT nome, count(*) AS qtd, agrega_histograma(*) AS histograma
FROM tbl_pessoas
GROUP BY nome
ORDER BY qtd, nome NULLS FIRST;
